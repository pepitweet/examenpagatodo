package com.pagatodo.examen

import android.content.Intent
import android.content.pm.ActivityInfo
import android.content.pm.PackageInfo
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.Window
import android.widget.Toast
import com.pagatodo.examen.vistas.MarcasList
import kotlinx.android.synthetic.main.login_activity.*

class MainActivity : AppCompatActivity() {

//    var lblVersion: TextView? = null
//    var txtUsuario: TextInputEditText? = null
//    var txtPass: TextInputEditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        setContentView(R.layout.login_activity)

        try {
            var pInfo: PackageInfo = this.packageManager.getPackageInfo(packageName, 0)
            lblVersion.text = pInfo.versionName
        } catch (e: Exception) {
        }
    }

    fun btnIniciar_click(v: View){
        var i:Intent = Intent(this, MarcasList::class.java)
        startActivity(i)
        return
    }
}