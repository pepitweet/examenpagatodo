package com.pagatodo.examen

import android.app.Application
import android.content.Context
import android.support.multidex.MultiDex

/**
 * Created by jrobe on 27/01/2018.
 */
class MainAplication : Application() {

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        MainAplication.appContext = applicationContext
    }

    companion object {
        var appContext: Context? = null
            private set
    }

}