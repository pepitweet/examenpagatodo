package com.pagatodo.examen.vistas

import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.support.design.widget.FloatingActionButton
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.pagatodo.examen.R
import com.pagatodo.examen.data.MarcasMovil
import com.pagatodo.examen.recycler.MarcasAdaper
import kotlinx.android.synthetic.main.activity_list_marcas.*
import kotlinx.android.synthetic.main.content_marcas.*

/**
 * Created by jrobe on 27/01/2018.
 */
class MarcasList:AppCompatActivity(){
    private var marcas:List<MarcasMovil>? = null
//    private var reciclador: RecyclerView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_marcas)

        toolbar.title = "Marcas"
        setSupportActionBar(toolbar)

        fab.setOnClickListener {
            val i = Intent(this@MarcasList, ModificarMarcas::class.java)
            startActivity(i)
        }

    }

    override fun onResume() {
        super.onResume()
        updateAdapter()
    }

    private fun updateAdapter() {
        marcas = MarcasMovil().findAll()
        reciclador.adapter = null
        if (marcas != null) {
            if (marcas!!.size > 0) {
                val adapter = MarcasAdaper(marcas!!, this)
                reciclador.layoutManager = GridLayoutManager(this, 3)
                reciclador.adapter = adapter
            } else {
                Toast.makeText(this, "No se encotraron categorías, agregue una para poder continuar", Toast.LENGTH_SHORT).show()
            }
        } else {
            Toast.makeText(this, "No se encotraron categorías, agregue una para poder continuar", Toast.LENGTH_SHORT).show()
        }
    }
}