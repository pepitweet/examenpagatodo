package com.pagatodo.examen.vistas

import android.app.Dialog
import android.content.pm.ActivityInfo
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.pagatodo.examen.R
import com.pagatodo.examen.data.MarcasMovil
import kotlinx.android.synthetic.main.activity_recarga.*

/**
 * Created by jrobe on 27/01/2018.
 */
class RecargarActivity:AppCompatActivity() {

    var idMarca:Int = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        setContentView(R.layout.activity_recarga)

        try{
            idMarca = intent.getIntExtra("idMarca", -1)
        }catch (e:Exception){}

        fillLogo()
    }

    fun fillLogo(){
        if(idMarca > 0) {
            var marcaDto: MarcasMovil? = MarcasMovil().findByPrimaryKey(idMarca)
            if(marcaDto!= null){
                try {
                    imvLogo.setImageBitmap(BitmapFactory.decodeFile(marcaDto.logoMarca))
                }catch (e:Exception){}
            }
        }
    }

    fun btnIniciar_click(v: View){
        var msgError = ""
        if(txtTelefono.text.toString().trim().equals("")){
            msgError += "\n* Ingrese el número de teléfono"
        }else{
            if(txtTelefono.text.toString().length != 10){
                msgError += "\n* Ingrese un número válido"
            }
        }

        if(txtMonto.text.toString().trim().equals("")){
            msgError += "\n* Ingrese el monto a recargar"
        }

        if(!msgError.equals("")){
            Toast.makeText(this@RecargarActivity, "Revise lo siguiente: " + msgError, Toast.LENGTH_SHORT).show()
        }else{
            //Continuar

            var dialog = Dialog(this)
            dialog.setContentView(R.layout.dialog_custom_messaje)

            var numeroTel = dialog.findViewById<TextView>(R.id.lblTelefono)
            numeroTel.text = txtTelefono.text.toString()

            var logo = dialog.findViewById<ImageView>(R.id.imgLogo)
            if(idMarca > 0) {
                var marcaDto: MarcasMovil? = MarcasMovil().findByPrimaryKey(idMarca)
                if(marcaDto!= null){
                    try {
                        logo.setImageBitmap(BitmapFactory.decodeFile(marcaDto.logoMarca))
                    }catch (e:Exception){}
                }
            }

            var monto = dialog.findViewById<TextView>(R.id.lblMonto)
            monto.text = txtMonto.text.toString()

            var btnCancelar = dialog.findViewById<Button>(R.id.btnCancelar)
            btnCancelar.setOnClickListener{ dialog.dismiss() }

            var btnConfirmar = dialog.findViewById<Button>(R.id.btnConfirmar)
            btnConfirmar.setOnClickListener {
                dialog.dismiss()
                finalDialog()
            }

            dialog.show()
        }
    }

    fun finalDialog(){
        var dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_final)
        var btnConfirmar = dialog.findViewById<Button>(R.id.btnConfirmar)
        btnConfirmar.setOnClickListener {
            dialog.dismiss()
            finish()
        }
        dialog.show()

    }
}