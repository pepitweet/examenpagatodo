package com.pagatodo.examen.vistas.util

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.PorterDuff
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import com.jaredrummler.materialspinner.MaterialSpinner
import com.pagatodo.examen.R
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_editor_imagen.*
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.util.*

/**
 * Created by jrobe on 27/01/2018.
 */
class EditorImagenActivity: AppCompatActivity() {
    companion object {
        private val FINISH_REQUEST_CODE = 1101
        private var EXTRA_URI = "https://pp.vk.me/c637119/v637119751/248d1/6dd4IPXWwzI.jpg"
    }

    private val qualityArray = ArrayList<String>()
    private val shapeArray = ArrayList<String>()
    private var rotateDegrees = 0
    private var qualityCompress = 0
    private var parent: String? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_editor_imagen)

        initControls()
        initEvents()

        val i = intent
        if (i.hasExtra("ImageUri")) {
            if (i.getStringExtra("ImageUri") != null && i.getStringExtra("ImageUri") != "")
                EXTRA_URI = i.getStringExtra("ImageUri")
        }
        if (i.hasExtra("Parent")) {
            parent = i.getStringExtra("Parent")
        }

        Log.v("EditorImagen", "Editar imagen uri: " + EXTRA_URI)
        val file = File(EXTRA_URI)
        if (file.exists()) {
            Log.v("EditorImagen", "Archivo existe")
            val myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath())
            Log.v("EditorImagen", "512.0 / " + myBitmap.width)
            val nh = (myBitmap.height * (512.0 / myBitmap.width)).toInt()
            val scaled = Bitmap.createScaledBitmap(myBitmap, 512, nh, true)
            cropImageView!!.setImageBitmap(scaled)
        } else {
            Log.v("EditorImagen", "Archivo no existe")
        }

        qualitySpinner!!.setItems(
                "Baja",
                "Media",
                "Alta",
                "Sin compresion")
        qualitySpinner!!.setSelectedIndex(3)
        qualitySpinner!!.setOnItemSelectedListener { view, position, id, item -> when (position) {
            0 -> qualityCompress = 30
            1 -> qualityCompress = 60
            2 -> qualityCompress = 90
            3 -> qualityCompress = 100
        } }
    }

    fun initControls(){
        setSupportActionBar(toolbarMain)
    }

    fun initEvents(){
        //<editor-fold defaultstate="collapsed" desc="Boton Rotate">
        btnRotate!!.setOnClickListener(View.OnClickListener {
            if (rotateDegrees + 90 > 360) {
                rotateDegrees = 0
            } else {
                rotateDegrees += 90
            }
            Log.v("EditorImagen", "Rotate degrees: " + rotateDegrees)
            cropImageView!!.setRotatedDegrees(rotateDegrees)
        })
        btnRotate!!.setOnTouchListener(View.OnTouchListener { view, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> {
                    val v = view as ImageView
                    //overlay is black with transparency of 0x77 (119)
                    v.drawable.setColorFilter(0x77000000, PorterDuff.Mode.SRC_ATOP)
                    v.invalidate()
                }
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                    val v = view as ImageView
                    //clear the overlay
                    v.drawable.clearColorFilter()
                    v.invalidate()
                }
            }
            false
        })
        //</editor-fold>

        //<editor-fold defaultstate="collapsed" desc="Bonton siguiente">

        btnSiguiente!!.setOnClickListener(View.OnClickListener {

            //Guardaremos la imagen en una carpeta y retornaremos la uri de esta para que se pueda guardar en la base de datos

            var image = saveImage()
            val returnIntent = Intent()
            returnIntent.putExtra("rutaImagen", image)
            setResult(Activity.RESULT_OK, returnIntent)
            finish()


        })

        //</editor-fold>
    }

    private fun getRealPathFromURI(contentURI: Uri): String {
        val result: String
        val cursor = contentResolver.query(contentURI, null, null, null, null)
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.path
        } else {
            cursor.moveToFirst()
            val idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA)
            result = cursor.getString(idx)
            cursor.close()
        }
        return result
    }

    fun saveImage(): String {
        val imgLogo = cropImageView!!.getCroppedImage()
        Log.v("EditarImagen", "Width: " + imgLogo.width)
        Log.v("EditarImagen", "Height: " + imgLogo.height)
        var fOut: OutputStream? = null
        var outputFileUri = ""
        try {
            val root = File(Environment.getExternalStorageDirectory().toString()
                    + File.separator + "Invoice4YouLogos" + File.separator)
            root.mkdirs()
            val sdImageMainDirectory = File(root, getImageName())
            outputFileUri = sdImageMainDirectory.absolutePath
            fOut = FileOutputStream(sdImageMainDirectory)
        } catch (e: Exception) {
            Toast.makeText(this, "Error al guardar la imagen",
                    Toast.LENGTH_SHORT).show()
        }

        try {
            imgLogo.compress(Bitmap.CompressFormat.PNG, qualityCompress, fOut)
            fOut!!.flush()
            fOut.close()
        } catch (e: Exception) {
            Log.v("CrearLogotipo", "Excepcion imagen: " + e.toString())
        }

        Log.v("CrearLogotipo", "Ruta imagen: " + outputFileUri)
        return outputFileUri
    }

    fun getImageName(): String {
        val c = Calendar.getInstance()
        val DIA = c.get(Calendar.DAY_OF_MONTH).toString()
        val MES = c.get(Calendar.MONTH).toString()
        val ANO = c.get(Calendar.YEAR).toString()
        val HORA = c.get(Calendar.HOUR).toString()
        val MINUTO = c.get(Calendar.MINUTE).toString()
        val SEGUNDO = c.get(Calendar.SECOND).toString()

        val nombre = DIA + MES + ANO + "_" + HORA + MINUTO + SEGUNDO + ".png"
        Log.v("CrearLogotipo", "Nombre imagen: " + nombre)

        return nombre
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {

        if (requestCode == FINISH_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                val returnIntent = Intent()
                setResult(Activity.RESULT_OK, returnIntent)
                finish()
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }
}