package com.pagatodo.examen.vistas

import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import com.pagatodo.examen.R
import com.pagatodo.examen.vistas.util.EditorImagenActivity
import kotlinx.android.synthetic.main.nueva_marca_activity.*
import java.io.File
import java.util.*
import android.support.v4.content.FileProvider
import android.os.Build
import android.view.Menu
import android.view.MenuItem
import com.pagatodo.examen.BuildConfig
import com.pagatodo.examen.data.MarcasMovil


/**
 * Created by jrobe on 27/01/2018.
 */
class ModificarMarcas : AppCompatActivity() {

    private val RESULT_LOAD_IMAGE:Int = 1000
    private val REQUEST_TACKE_CAMERA_PICTURE = 1002
    private val REQUEST_MODIFICAR_IMAGE = 1001

    private val MY_PERMISSIONS_REQUEST_STORAGE = 10004
    private val MY_PERMISSIONS_CAMERA = 10005

    private var rutaIcono:String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.nueva_marca_activity)

        initEvents()
    }

    private fun initEvents() {
        imvFoto.setOnClickListener{
            val alert = AlertDialog.Builder(this@ModificarMarcas)
            alert.setTitle("Cargar icono...")
            alert.setMessage("¿De dónde desea cargar el icono de la categoria?")
            alert.setPositiveButton("Cámara") { dialog, which -> run{
                if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), MY_PERMISSIONS_CAMERA)
                }else {
                    getPictureCamera()
                }
            } }
            alert.setNegativeButton("Galería") { dialog, which -> run{
                if(ContextCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                    ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.READ_EXTERNAL_STORAGE), MY_PERMISSIONS_REQUEST_STORAGE)
                }else {
                    getPictureGalery()
                }
            } }
            alert.show()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.modificar_marca_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item!!.itemId) {
            R.id.itemGuardar -> guardar()
        }
        return true
    }

    var imageName:String = ""
    val imageFolderPath = Environment.getExternalStorageDirectory().toString() + "/Camera"
    private fun getPictureCamera(){
        val imagesFolder = File(imageFolderPath)
        imagesFolder.mkdirs()

        // Generating file name
        var output: Uri? = null
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            output = FileProvider.getUriForFile(this, BuildConfig.APPLICATION_ID + ".provider", File(imageFolderPath, imageName))
        } else {
            output = Uri.fromFile(File(imageFolderPath, imageName))
        }
        imageName = Date().toString() + ".png"
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        intent.putExtra(MediaStore.EXTRA_OUTPUT, output)
        intent.putExtra(MediaStore.EXTRA_SHOW_ACTION_ICONS, false)
        startActivityForResult(intent, REQUEST_TACKE_CAMERA_PICTURE)
    }

    private fun getPictureGalery(){
        val i = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(i, RESULT_LOAD_IMAGE)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == MY_PERMISSIONS_CAMERA){
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPictureCamera()
            }else{
                Toast.makeText(this@ModificarMarcas, "No se otorgaron permisos", Toast.LENGTH_SHORT).show()
            }
        }
        if(requestCode == MY_PERMISSIONS_REQUEST_STORAGE){
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getPictureGalery()
            }else{
                Toast.makeText(this@ModificarMarcas, "No se otorgaron permisos", Toast.LENGTH_SHORT).show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            var selectedImage:Uri = data.data
            var filePathColumn = Array<String>(1) {MediaStore.Images.Media.DATA}
            var cursor:Cursor = contentResolver.query(selectedImage, filePathColumn, null, null, null)
            cursor.moveToFirst()
            var columnIndex:Int = cursor.getColumnIndex(filePathColumn[0])
            var picturePath:String = cursor.getString(columnIndex)
            cursor.close()
//            imvIcono!!.setImageBitmap(BitmapFactory.decodeFile(picturePath))
            var intent = Intent(this, EditorImagenActivity::class.java)
            intent.putExtra("ImageUri", picturePath)
            startActivityForResult(intent, REQUEST_MODIFICAR_IMAGE)
        }
        if(requestCode == REQUEST_MODIFICAR_IMAGE){
            if(resultCode == Activity.RESULT_OK){
                try {
                    rutaIcono = data!!.getStringExtra("rutaImagen")
                    imvFoto.setImageBitmap(BitmapFactory.decodeFile(rutaIcono))
                }catch (e:Exception){
                    e.printStackTrace()
                    rutaIcono = ""
                    imvFoto.setImageDrawable(resources.getDrawable(android.R.drawable.ic_menu_camera))
                }
            }else{
                Toast.makeText(this, "Se canceló la modificación de la imagen", Toast.LENGTH_SHORT).show()
            }
        }
        if(requestCode == REQUEST_TACKE_CAMERA_PICTURE){
            if(resultCode == Activity.RESULT_OK){
                var fi:File = File(imageFolderPath, imageName)
                if(fi.exists()){
                    var intent = Intent(this, EditorImagenActivity::class.java)
                    intent.putExtra("ImageUri", fi.absolutePath)
                    startActivityForResult(intent, REQUEST_MODIFICAR_IMAGE)
                }else{
                    Toast.makeText(this, "No se econtró la imagen", Toast.LENGTH_SHORT).show()
                }
            }else{
                Toast.makeText(this, "Proceso cancelado por el usuario", Toast.LENGTH_SHORT).show()
            }
        }

    }

    private fun guardar() {
        try{
            val marcaStr:String = txtNombre.text.toString()
            val descripcion:String = txtDescripcion.text.toString()

            var msgError = ""
            if (marcaStr.trim { it <= ' ' } == "")
                msgError += "\n* Debe ingresar el nombre de la categoría."
            if (descripcion.trim { it <= ' ' } == "")
                msgError += "\n* Debe ingresar la descripción de la categpría."

            if (msgError.trim { it <= ' ' } == "") {
                var marcaDto = MarcasMovil()

                marcaDto.idMarca = -1
                marcaDto.nombreMarca = marcaStr
                marcaDto.descrpcionMarca = descripcion

                marcaDto.logoMarca = rutaIcono

                MarcasMovil().getRepository().create(marcaDto)

                finish()
            }
        }catch (e:Exception){}
    }
}