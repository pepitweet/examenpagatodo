package com.pagatodo.examen.data

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.support.ConnectionSource
import com.j256.ormlite.table.TableUtils
import com.pagatodo.examen.MainAplication

/**
 * Created by jrobe on 27/01/2018.
 */
class DBHelper(context: Context, path: String) : OrmLiteSqliteOpenHelper(context, path, null, DATABASE_VERSION){
    var mContext:Context? = null

    class DaoGet<T>{
        companion object {
            operator fun <T> get(clazz: Class<T>) : RuntimeExceptionDao<T, Int> {
                return DBHelper.helper.getRuntimeExceptionDao<RuntimeExceptionDao<T, Int>, T>(clazz)
            }
        }
    }

    companion object {
        private val DATABASE_VERSION:Int = 1
        private var mInstance: DBHelper?= null
        val helper: DBHelper
            get() {
                if(mInstance == null) {
                    try {
                        mInstance = DBHelper(MainAplication.Companion.appContext!!, "/data/data/com.pagatodo.examen/examen_db.s3db")
                    }catch (e:Exception){
                        e.printStackTrace()
                    }
                }

                return mInstance!!
            }
    }




    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        TableUtils.createTableIfNotExists(connectionSource, MarcasMovil::class.java)
    }

    override fun onUpgrade(database: SQLiteDatabase?, connectionSource: ConnectionSource?, oldVersion: Int, newVersion: Int) {
        onCreate(database, connectionSource)
    }

}