package com.pagatodo.examen.data

import com.j256.ormlite.dao.RuntimeExceptionDao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.stmt.QueryBuilder
import com.j256.ormlite.table.DatabaseTable

/**
 * Created by jrobe on 27/01/2018.
 */
@DatabaseTable(tableName = "marcas_movil")
data class MarcasMovil(
        @DatabaseField(columnName = "id_marca", generatedId = true)var idMarca:Int = -1,
        @DatabaseField(columnName = "nombre_marca") var nombreMarca:String = "",
        @DatabaseField(columnName = "descripcion_marca")var descrpcionMarca:String = "",
        @DatabaseField(columnName = "logo_ruta")var logoMarca:String = "",
        @DatabaseField(columnName = "id_estatus")var idEstatus:Int = 1) {

    constructor():this(-1,"","","",1)

    fun getRepository() : RuntimeExceptionDao<MarcasMovil, Int>{
        return DBHelper.DaoGet.get(MarcasMovil::class.java)
    }

    fun findAll():List<MarcasMovil>{
        var lst:List<MarcasMovil> = ArrayList<MarcasMovil>()
        var qB: QueryBuilder<MarcasMovil, Int> = getRepository().queryBuilder()
        try{
            qB.orderBy("nombre_marca", true)
            lst = qB.query()
        }catch (e:Exception){
            e.printStackTrace()
        }
        return lst
    }

    fun findByPrimaryKey(id:Int): MarcasMovil?{
        var categoriaMovil:MarcasMovil? = null
        try {
            var lst:List<MarcasMovil> = ArrayList<MarcasMovil>()
            var qB:QueryBuilder<MarcasMovil, Int> = MarcasMovil().getRepository().queryBuilder()
            qB.where().eq("id_marca", id)
            lst = qB.query()
            if(lst.size > 0)
                categoriaMovil = lst.get(0)
        }catch (e:Exception){
            e.printStackTrace()
        }
        return categoriaMovil
    }


}