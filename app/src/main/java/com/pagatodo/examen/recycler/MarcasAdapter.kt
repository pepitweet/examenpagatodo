package com.pagatodo.examen.recycler

import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.pagatodo.examen.R
import com.pagatodo.examen.data.MarcasMovil
import com.pagatodo.examen.vistas.RecargarActivity

/**
 * Created by jrobe on 27/01/2018.
 */
class MarcasAdaper : RecyclerView.Adapter<MarcasAdaper.MarcasViewHolder>{
    companion object {
        var adapterContext: Context? = null
        var marcas:List<MarcasMovil>? = null
    }

    class MarcasViewHolder :RecyclerView.ViewHolder{
        var cardView: CardView? = null
        var imageView: ImageView? = null
        var textView1: TextView? = null
        constructor(v: View):super(v){
            cardView = v.findViewById<CardView>(R.id.cv) as CardView
            imageView = v.findViewById<ImageView>(R.id.icono_categoria) as ImageView
            textView1 = v.findViewById<TextView>(R.id.nombre_marca) as TextView

        }
    }

    constructor(items:List<MarcasMovil>, context: Context){
        adapterContext = context
        marcas = items
        if(marcas == null)
            marcas = ArrayList<MarcasMovil>()
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MarcasViewHolder {
        val v: View = LayoutInflater.from(parent!!.context).inflate(R.layout.custom_item_marcas, parent, false)
        return MarcasViewHolder(v)
    }

    override fun onBindViewHolder(holder: MarcasViewHolder?, position: Int) {
        if(marcas != null){
            if(marcas!!.size > 0){
                var nombre = marcas!!.get(position).nombreMarca
                holder!!.textView1!!.text = nombre
                try{
                    holder!!.imageView!!.setImageBitmap(BitmapFactory.decodeFile(marcas!!.get(position).logoMarca))
                }catch (e:Exception){}
                holder!!.cardView!!.setOnClickListener {
                    var i = Intent(adapterContext, RecargarActivity::class.java)
                    i.putExtra("idMarca", marcas!!.get(position).idMarca)
                    adapterContext!!.startActivity(i)
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return marcas!!.size
    }
}